#AssetOwl Developer Test

## Introduction

Please start by reading [this](https://docs.google.com/document/d/1w3L0sRZ9bbgizcJ1i2qvzZpJJd4Ka6LUHkZ33LK23c4/edit?usp=sharing), and thanks for considering AssetOwl!

## Setup

These instructions are for OSX, our preferred development environment. For other environments please consult the interwebs. And don't forget JDK 8!

- Install Brew

```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

- Install node

```
brew install npm
```

- Install grunt

```
npm install -g grunt-cli
```

- Execute

```
./gradlew npmInstall installGrunt gruntBuild
```

## Building

- Execute

```
./gradlew
```

## Running

- Execute

```
./gradlew runShadow
```
