/* global require, module */
module.exports = function (grunt) {
  'use strict';

  // Load grunt tasks automatically
  require('load-grunt-tasks')(grunt);

  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

    grunt.loadNpmTasks('grunt-symbolic-link');
  grunt.loadNpmTasks('grunt-webfont');
  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-run');
  grunt.loadNpmTasks('grunt-uncss');

  // Configurable paths for the application
  var appConfig = {
    app: require('./bower.json').appPath || 'app',
    dist: 'dist'
  };

  grunt.initConfig({

    // Project settings
    yeoman: appConfig,

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json'],
        tasks: ['wiredep']
      },
      js: {
        files: ['<%= yeoman.app %>/scripts/{,*/}*.js'],
        tasks: ['newer:jshint:all'],
        options: {
          livereload: '<%= connect.options.livereload %>'
        }
      },
      jsTest: {
        files: ['test/unit/{,*/}*.js'],
        tasks: ['newer:jshint:test', 'karma:unit']
      },
      sass: {
        files: ['<%= yeoman.app%>/styles/{,*/}*.{scss,sass}'],
        tasks: ['sass']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      html2js: {
        files: ['app/views/**/*.html'],
        tasks: ['html2js:main']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= yeoman.app %>/{,*/}*.html',
          '.tmp/styles/{,*/}*.css',
          '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },

    // The actual grunt server settings
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: 'localhost',
        livereload: 35729
      },
      livereload: {
        options: {
          open: true,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      test: {
        options: {
          port: 7001,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect.static('test'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      dist: {
        options: {
          open: true,
          base: '<%= yeoman.dist %>'
        }
      }
    },

    // Make sure code styles are up to par and there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= yeoman.app %>/scripts/{,*/}*.js'
        ]
      },
      test: {
        options: {
          jshintrc: 'tests/.jshintrc'
        },
        src: [
          'tests/{,**/}*.js'
        ]
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [
          {
            dot: true,
            src: [
              '.tmp',
              '<%= yeoman.dist %>/{,*/}*',
              '!<%= yeoman.dist %>/.git*'
            ]
          }
        ]
      },
      server: '.tmp'
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['> 1%', 'ie >= 10', 'Chrome >= 14', 'ff >= 14', 'Safari >= 5.1', 'Opera >= 12', 'last 2 iOS versions', 'last 2 Android versions']
      },
      dist: {
        files: [
          {
            expand: true,
            cwd: '.tmp/styles/',
            src: '{,*/}*.css',
            dest: '.tmp/styles/'
          }
        ]
      }
    },

    // Automatically inject Bower components into the app
    wiredep: {
      options: {
        cwd: ''
      },
      app: {
        src: ['<%= yeoman.app %>/index.html'],
        exclude: [
            'bower_components/jquery/dist/jquery.js',
            'bower_components/angular/angular.js',

        ],
        ignorePath: /\.\.\//
      },
      sass: {
        src: ['<%= yeoman.app %>/styles/{,*/}*.{scss,sass}'],
        ignorePath: /(\.\.\/){1,2}bower_components\//
      }
    },
    sass: {
      options: {
        sourceMap: false
      },
      dist: {
        files: {
          '.tmp/styles/main.css': '<%= yeoman.app %>/styles/main.scss'
        }
      }
    },
    // Renames files for browser caching purposes
    filerev: {
      dist: {
        src: [
          '<%= yeoman.dist %>/scripts/{,*/}*.js',
          '<%= yeoman.dist %>/styles/{,*/}*.css'
        ]
      }
    },
    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: '<%= yeoman.app %>/index.html',
      options: {
        dest: '<%= yeoman.dist %>',
        flow: {
          html: {
            steps: {
              js: ['concat', 'uglifyjs'],
              css: ['cssmin']
            },
            post: {}
          }
        }
      }
    },
    uglify: {
      options: {
        sourceMap: false,
        sourceMapIncludeSources: false
      }
    },
    // Performs rewrites based on filerev and the useminPrepare configuration
    usemin: {
      html: ['<%= yeoman.dist %>/{,*/}*.html'],
      css: ['<%= yeoman.dist %>/styles/{,*/}*.css'],
      options: {
        assetsDirs: ['<%= yeoman.dist %>', '<%= yeoman.dist %>/images'],
        blockReplacements: {
          include: function (block) {
            return '<script type="text/javascript">\n' + grunt.file.read(block.dest) + '\n</script>';
          }
        }
      }
    },
    imagemin: {
      dist: {
        files: [
          {
            expand: true,
            cwd: '<%= yeoman.app %>/images',
            src: '{,*/}*.{png,jpg,jpeg,gif}',
            dest: '<%= yeoman.dist %>/images'
          }
        ]
      }
    },

    svgmin: {
      options: {
        plugins: [
          {
            mergePaths: false
          }
        ]
      },
      dist: {
        files: [
          {
            expand: true,
            cwd: '<%= yeoman.app %>/images',
            src: '{,*/}*.svg',
            dest: '<%= yeoman.dist %>/images'
          }
        ]
      }
    },
    ngAnnotate: {
      options: {
        singleQuotes: true
      },
      dist: {
        files: [
          {
            expand: true,
            cwd: '.tmp/concat/scripts',
            src: '*.js',
            dest: '.tmp/concat/scripts'
          }
        ]
      }
    },
    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [
          {
            expand: true,
            dot: true,
            cwd: '<%= yeoman.app %>',
            dest: '<%= yeoman.dist %>',
            src: [
              '*.{ico,png,txt,pdf}',
              '.htaccess',
              '*.html',
              'images/{,*/}*.{webp}',
              'styles/fonts/*',
              'media/*'
            ]
          },
          {
            expand: true,
            cwd: '.tmp/images',
            dest: '<%= yeoman.dist %>/images',
            src: ['generated/*']
          },
          {
            expand: true,
            cwd: '.',
            src: 'bower_components/bootstrap-sass-official/assets/fonts/bootstrap/*',
            dest: '<%= yeoman.dist %>'
          }
        ]
      },
      styles: {
        expand: true,
        cwd: '<%= yeoman.app %>/styles',
        dest: '.tmp/styles/',
        src: '{,*/}*.css'
      },
      images: {
        expand: true,
        cwd: '<%= yeoman.app %>/images',
        dest: '<%= yeoman.dist %>/images/',
        src: '{,*/}*.{png,jpg,jpeg,gif,svg}'
      }
    },
    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'sass:dist'
      ],
      test: [],
      dist: [
        'buildJs',
        'buildCss'
      ],
    },

    // Test settings
    karma: {
      unit: {
        configFile: 'tests/karma.conf.js',
        singleRun: true
      },
      autotest: {
        configFile: 'tests/karma.conf.js',
        autoWatch: true,
        reporters: ['dots']
      }
    },
    html2js: {
      options: {
        base: 'app',
        module: 'uiIr.templates',
        singleModule: true,
        quoteChar: '\'',
        useStrict: true,
        htmlmin: {
          collapseBooleanAttributes: true,
          collapseWhitespace: true,
          removeAttributeQuotes: true,
          removeComments: true,
          removeEmptyAttributes: true,
          removeRedundantAttributes: true,
          removeScriptTypeAttributes: true,
          removeStyleLinkTypeAttributes: true
        }
      },
      main: {
        src: ['app/views/**/*.html'],
        dest: 'app/scripts/populate_template_cache.js'
      }
    }
  });

  grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
    if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
    }

    grunt.task.run([

      'clean:server',
      'wiredep',
      'concurrent:server',
      'autoprefixer',
      'html2js:main',
      'connect:livereload',
      'watch'
    ]);
  });

  grunt.registerTask('test', [
    'jshint',
    //'clean:server',
    'html2js:main',
    'concurrent:test',
    'connect:test',
    'karma:unit'
  ]);

  grunt.registerTask('buildJs', [
    'useminPrepare',
    'html2js:main',
    'concat',
    //'ngAnnotate',
    'uglify'
  ]);

  grunt.registerTask('buildCss', [
    'useminPrepare',
    'sass:dist',
    'autoprefixer',
    'cssmin'
  ]);

  grunt.registerTask('build', [
    'jshint:all',
    'clean:dist',
    'wiredep',
    'useminPrepare',
    'concurrent:dist',
    'copy:dist',
    'copy:images',
    'usemin'
  ]);

  grunt.registerTask('minifyImages', [
    'imagemin',
    'svgmin'
  ]);

  grunt.registerTask('autotest', [
    'karma:autotest'
  ]);

  grunt.registerTask('default', [
    'test',
    'build'
  ]);
    grunt.registerTask('continue', [
    ]);

};
