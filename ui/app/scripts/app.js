(function () {
    'use strict';

    angular.module('ui', ['ngResource', 'ngRoute', 'ngStorage'])
        .constant('UI_CONFIG',{
            serviceEndPoints: {
                'ui': 'http://localhost:9080'
            }
        })
        .config(function ($routeProvider) {

            $routeProvider
                .when('/dashboard', {
                    templateUrl: 'views/dashboard.html',
                    controller: 'DashboardCtrl',
                    resolve: {
                        access: ['Access', function(Access) { return Access.isAuthenticated(); }],
                    }
                })
                .when('/login',{
                    templateUrl:'views/login.html',
                    controller:'LoginCtrl'
                })
                // TODO: Change me.
                .otherwise({redirectTo:'/dashboard'});
        })
        .run(['$rootScope', '$location', function($rootScope, $location) {
            $rootScope.$on('$routeChangeError', function() {
                $location.path('/login');
            });
        }]);
})();


