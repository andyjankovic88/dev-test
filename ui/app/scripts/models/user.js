angular.module('ui').factory('User', ['$http', '$q', '$localStorage', 'UI_CONFIG', 
	function($http, $q, $localStorage, UI_CONFIG) {
		'use strict';
		var userProfile = null;

	return {
		doLogin: function (data) {
			if (data) {
				data.token = $localStorage.token;
			}
			// return $http.post(UI_CONFIG.serviceEndPoints.ui +'/login/', data)
			return $http.get(UI_CONFIG.serviceEndPoints.ui +'/welcome/890ukjdbmn98jk')
				.success(function () {
					//$localStorage.token = res.data.token;
					$localStorage.token = '1234567asdf';
					userProfile = {
						name: 'andy',
						phone: '231-3423'
					};
				});
		},
		getProfile: function () {
			var deferred = $q.defer();
			if (userProfile) {
				deferred.resolve(userProfile);
			} else if ($localStorage.token) {
				//return $http.get(UI_CONFIG.serviceEndPoints.ui +'/profile/me')
				$http.get(UI_CONFIG.serviceEndPoints.ui +'/welcome/890ukjdbmn98jk')
					.success(function () {
						// $localStorage.token = res.data.token;
						// userProfile = res.data.profile;
						$localStorage.token = '1234567asdf';
						userProfile = {
							name: 'andy',
							phone: '231-3423'
						};
						deferred.resolve(userProfile);
					});
			} else {
				deferred.reject('noToken');
			}
			return deferred.promise;
		},
		doLogout: function () {
			// return $http.post(UI_CONFIG.serviceEndPoints.ui +'/logout/', {token: $localStorage.token})
			$localStorage.token = null;
			userProfile = null;
			return $http.get(UI_CONFIG.serviceEndPoints.ui +'/welcome/890ukjdbmn98jk')
				.success(function () {
					$localStorage.token = null;
				});
		}
	};
}]);