angular.module('ui').controller('LoginCtrl',
    ['$scope', '$location', 'User', function ($scope, $location, User) {
        'use strict';
        $scope.text = 'TODO';
        $scope.doLogin = function () {
            if ($scope.LoginForm.$valid) {
                User.doLogin().success(function () {
                	$location.path('/dashboard');
                }).error (function () {
                	alert('Login Failed.');
                });
            } else {
                alert('please input username and password.');
            }
        };
    }]
);
