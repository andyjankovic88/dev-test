angular.module('ui').controller('DashboardCtrl',
    function ($scope,$http, $location, UI_CONFIG, User) {
        'use strict';
        // TODO: Change me!
        $http.get(UI_CONFIG.serviceEndPoints.ui +'/welcome/890ukjdbmn98jk').then(function(object) { //jshint ignore: line
            $scope.message = object.data.message;
        });

        $scope.logout = function () {
        	User.doLogout().success(function () {
        		$location.path('/login');
        	});
        };
    });
