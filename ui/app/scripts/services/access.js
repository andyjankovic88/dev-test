angular.module('ui').factory('Access', ['$q', 'UserProfile',
function($q, UserProfile) {
	'use strict';

	var Access = {
		OK: 200,
		UNAUTHORIZED: 401,

		isAuthenticated: function() {
			var deferred = $q.defer();
			UserProfile.getAuthorize().then(function(userProfile) {
				if (userProfile.isAuthenticated()) {
					deferred.resolve(Access.OK);
				} else {
					deferred.reject(Access.UNAUTHORIZED);
				}
			}, function () {
				deferred.reject(Access.UNAUTHORIZED);
			});
			return deferred.promise;
		}
	};

	return Access;
}]);