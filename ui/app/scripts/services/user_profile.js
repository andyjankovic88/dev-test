angular.module('ui').factory('UserProfile', ['$q', 'User',
function($q, User) {
	'use strict';
	return {
		getAuthorize: function () {
			var deferred = $q.defer();
			User.getProfile().then(function(userProfile) {
				deferred.resolve({
					isAuthenticated: function() {
						return userProfile && userProfile.name;
					}
				});
			},function () {
				deferred.reject('NoAuthorized');
			});
			return deferred.promise;
		}
	};
}]);