/*
 * Copyright (C) 2015, Asset View Pty. Ltd., all rights reserved.
 */

package com.assetowl.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Welcome {

    @JsonProperty
    private String message;

    public Welcome(final String message) {
        this.message = message;
    }

}
