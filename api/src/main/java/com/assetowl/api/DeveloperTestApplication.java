/*
 * Copyright (C) 2015, Asset View Pty. Ltd., all rights reserved.
 */

package com.assetowl.api;

import com.assetowl.api.resources.WelcomeResource;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class DeveloperTestApplication extends Application<Configuration> {

	public static void main(String[] args) throws Exception {
		new DeveloperTestApplication().run(args);
	}

	@Override
	public void run(final Configuration informationRequestConfiguration, final Environment environment) throws Exception {

		enableCors(environment);

		environment.jersey().register(new WelcomeResource());

	}


	private void enableCors(final Environment environment) {

		// Enable CORS headers - note Origin: http://foo.example header must be sent for these headers to be output.
		final FilterRegistration.Dynamic cors = environment.servlets().addFilter("CORS", CrossOriginFilter.class);

		// Configure CORS parameters
		cors.setInitParameter("allowedOrigins", "*");
		cors.setInitParameter("allowedHeaders", "Origin,Authorization,Content-Type,Accept");
		// Cannot have spaces after commas!
		cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

		// Add URL mapping
		cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

	}


}