/*
 * Copyright (C) 2015, Asset View Pty. Ltd., all rights reserved.
 */

package com.assetowl.api.resources;


import com.assetowl.api.model.Welcome;
import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/welcome")
@Produces(MediaType.APPLICATION_JSON)
public class WelcomeResource {

    // For this test it's fine to store the valid security code as a constant within the code (but it does not have to remain in this resource).
    private static final String VALID_SECURITY_CODE = "890ukjdbmn98jk";

    public WelcomeResource() { }

    @Timed
    @GET
    @Path("/{security}")
    // For this test, it is fine to pass the security parameter on the URL - why should this never be done in the real world?
    public Welcome getWelcome(@PathParam("security") final String security) {

        if (VALID_SECURITY_CODE.equals(security)) {

            return new Welcome("Welcome Developer!");

        } else {

            throw new WebApplicationException(Response.status(Response.Status.UNAUTHORIZED).build());

        }

    }


}
