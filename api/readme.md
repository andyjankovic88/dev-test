#AssetOwl Developer Test API


##Prerequisites
- JDK 8



## Running API server locally from command line

Execute

```
./gradlew runShadow
```

Then browse to [localhost:9080/welcome/890ukjdbmn98jk](http://localhost:9080/welcome/890ukjdbmn98jk).

Note: the console may pause at around 85% but the server is probably running!



## IntelliJ

- IMPORT the project into IntelliJ (do not use open) and select use *customizable gradle wrapper* - if have problems ensure the Module Settings are set to use JDK 8 as the project and module SDK and language level


## Running API server locally from IntelliJ

- Create run Config for main application class `DeveloperTestApplication` with gradle task `clean build` using the ROOT (not api/build.gradle) build.gradle file, set script parameters to be `-x test` and set program arguments to

```
server api/src/main/resources/config.yml
```




## Solving IntelliJ problems

- If get the error

```
Execution failed for task ':api:compileJava'.
                 > invalid source release: 1.8
```

check File > Other Settings > Default Settings > Java Compiler > bytecode version is set to 1.8


- If get error

```
Diamond operator is not supported in -source 1.6
```

right click on assetowl folder, Open Module Settings > Project > Project Language > set to 1.8.

- If you get the IntelliJ error when trying to execute the server 

```
java.lang.ClassNotFoundException: com.mysql.jdbc.Driver
```

Then in IntelliJ refresh the Gradle project.


- If having weird IntelliJ compile build errors then File > Invalidate Caches / Restart > Invalidate and Restart 